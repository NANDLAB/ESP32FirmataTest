#include <iostream>
#include "firmata/client.hpp"
#include "firmata/serial_port.hpp"

using namespace firmata::literals;
using namespace std::chrono_literals;

using namespace std;
const uint8_t switches[] = {32, 33, 25, 26, 27, 18, 4, 13};
const uint8_t magnets[] = {23, 22, 19, 5, 14, 12, 2, 15};

asio::io_context ioc;
boost::asio::steady_timer timer(ioc);
firmata::client *client;

void timer_handler(const boost::system::error_code& e)
{
  for (int i = 0; i < 8; i++) {
    cout << client->pin(switches[i]).state() << " ";
  }
  cout << endl;
  timer.expires_after(boost::asio::chrono::seconds(1));
  timer.async_wait(timer_handler);
}

/*
void input_pin_handler(int pin) {
    for (int i = 0; i < 8; i++) {
      cout << client->pin(switches[i]).state() << " ";
    }
    cout << endl;
}
*/

void input_pin_rise();

void input_pin_fall() {
    for (int i = 0; i < 8; i++) {
      cout << client->pin(switches[i]).state() << " ";
    }
    cout << endl;
    for (int i = 0; i < 8; i++) {
        // arduino.pin(i).on_state_changed(input_pin_handler);
        // client->pin(i).on_state_low(input_pin_fall);
        client->pin(i).on_state_high(input_pin_rise);
    }
}

void input_pin_rise() {
    for (int i = 0; i < 8; i++) {
      cout << client->pin(switches[i]).state() << " ";
    }
    cout << endl;
    for (int i = 0; i < 8; i++) {
        // arduino.pin(i).on_state_changed(input_pin_handler);
        client->pin(i).on_state_low(input_pin_fall);
        // client->pin(i).on_state_high(input_pin_rise);
    }
}

int main()
{
    cout << "main: start" << endl;
    firmata::serial_port device(ioc, "/dev/ttyUSB0"); // "/dev/ttyACM0"
    device.set(115200_baud);
    firmata::client arduino(device);
    client = &arduino;
    cout << "arduino object created." << endl;
    cout << "Pin 5 mode: " << arduino.pin(5).mode() << endl;
    cout << "Setting magnet pins to output..." << endl;
    for (int i = 0; i < 8; i++) {
        arduino.pin(magnets[i]).mode(mode::digital_out);
    }
    cout << "Setting switch pins to pullup input..." << endl;
    for (int i = 0; i < 8; i++) {
        arduino.pin(switches[i]).mode(mode::pullup_in);
    }
    cout << "All pins set." << endl;
    for (int j = 0; j < 2; j++) {
        for (int i = 0; i < 8; i++) {
            arduino.pin(magnets[i]).value(1);
            usleep(250000);
            arduino.pin(magnets[i]).value(0);
        }
    }
    timer.async_wait(timer_handler);
    /*
    while (1) {
        cout << arduino.pin(IN).state() << endl;
        sleep(1);
    }
    */
    timer.expires_after(boost::asio::chrono::seconds(1));
    // arduino.pin(i).on_state_low([](){ cout << "low" << endl; });
    for (int i = 0; i < 8; i++) {
        // arduino.pin(i).on_state_changed(input_pin_handler);
        arduino.pin(switches[i]).on_state_low([](){ cout << "low" << endl; });
        arduino.pin(switches[i]).on_state_high([](){ cout << "high" << endl; });
    }
    ioc.run();
    cout << "main: end" << endl;
    return 0;
}
